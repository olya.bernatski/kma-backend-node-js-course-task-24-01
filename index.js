const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.PORT || 56201;

app.use(bodyParser.text());

app.post('/square', (req, res) => {
    const contentType = req.headers['content-type'];
    if (contentType && contentType.toLowerCase() === 'text/plain') {
        const number = +req.body;
        res.json({
            "number": number,
            "square": number*number
        });

    } else {
        res.status(400).send('Wrong content type');
    }

});

app.post('/reverse', (req, res) => {
    const contentType = req.headers['content-type'];
    if (contentType && contentType.toLowerCase() === 'text/plain') {
        const reversedText = req.body.split('').reverse().join('');
        res.setHeader("Content-Type", "text/plain");
        res.send(reversedText);
    } else {
        res.status(400).send('Wrong content type');
    }

});

app.get('/date/:year/:month/:day', (req, res) => {
    const {year, month, day} = req.params;

    const date = new Date(year, month - 1, day);
    const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const weekDay = daysOfWeek[date.getDay()];
    const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
    let today = new Date()
    let difference = Math.abs(Math.floor((today - date) / (1000 * 60 * 60 * 24)))
    res.json({
        weekDay, isLeapYear, difference
    });
})

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
